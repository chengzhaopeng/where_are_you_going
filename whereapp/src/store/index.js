import Vue from "vue"
import Vuex from "vuex"
Vue.use(Vuex);
let defauft = localStorage.city?localStorage.city:"西安";
let state = {
  city:defauft
};
let mutations = {
  changeName(state,cityName){
    state.city = cityName;
    localStorage.city = cityName;
  }
};
export default new Vuex.Store({
  state,//抛出state的内容
  mutations
})
