import Vue from 'vue'
import Router from 'vue-router'
// import index from '@/components/index'
import Home from '../components/Home/Home.vue'
import City from '../components/city/City.vue'
import Details from '../components/details/Details.vue'
Vue.use(Router);
export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },{
      path: '/city',
      name: 'City',
      component: City
    },{
      path: '/details',
      name: 'Details',
      component: Details
    }
  ]
})
